package net.gnejs.tree

import akka.actor._
import akka.util.Duration

import akka.util.duration._
import akka.remote.RemoteClientWriteFailed

// define messages.

package cluster {

import java.util.Date
import java.util.concurrent.TimeUnit
import com.hazelcast.core.{Hazelcast, HazelcastInstance}
import akka.remote.RemoteScope

class Cluster(sys: ExtendedActorSystem, hz: HazelcastInstance) extends Extension {
  val random = new java.security.SecureRandom()
  val map = hz.getMap[Address, Date]("clustered_actor_systems")
  val myAddress = sys.provider.getExternalAddressFor(Address("akka", sys.name, "", 0))
    .getOrElse(throw new Exception("Why no address?"))

  // Register the deregistration.
  sys.registerOnTermination(detach())

  // Register the update proc.
  val cancellable = Some(sys.scheduler.schedule(0.second, 5.seconds){attach()})


  def attach(){
    println("hz: Updating " + myAddress)
    map.put(myAddress, new Date(), 7, TimeUnit.SECONDS)
  }
  def detach() {
    cancellable.foreach(_.cancel())
    map.remove(myAddress)
  }

  def clusterMembers = map.keySet()
  def randomMember: Address = {
    if (map.isEmpty) myAddress
    else {
      val list = new java.util.ArrayList[Address](map.keySet())
      java.util.Collections.shuffle(list, random)
      list.get(0)
    }
  }
}
object ClusterExtension extends ExtensionId[Cluster] with ExtensionIdProvider {
  lazy val hz = Hazelcast.getDefaultInstance
  Runtime.getRuntime.addShutdownHook(new Thread(new Runnable {
    def run() {hz.getLifecycleService.shutdown()}
  }))
  def lookup = ClusterExtension
  def createExtension(sys: ExtendedActorSystem) = new Cluster(sys, hz)
}
trait ClusterSupport { self: Actor =>
  def clusterDeploy = Deploy(scope = RemoteScope(ClusterExtension(context.system).randomMember))
}
}


/**
 * Sent to a Node instructing it to create its child nodes.
 * @param width the number of child nodes to create.
 * @param depth the number of levels to create.
 */
case class Create(width: Int, depth: Int)
case object Ping
case class Pong(count: Int)


class Node extends Actor with ActorLogging with cluster.ClusterSupport {

  case class Delayed(msg: Any, recipient: ActorRef)

  case class PongStatus(childsLeft: Int,  totalCount: Int) {
    def touch(count: Int) = copy(childsLeft - 1, totalCount + count)
  }




  var pongStatus: Option[PongStatus] = None

  def sleep: PartialFunction[Any, Any] = {
    case x => {
      Thread.sleep(1000)
      x
    }
  }
  def trace: PartialFunction[Any, Any] = {
    case x => {
      log.info("Message: {}", x)
      x
    }
  }

  protected def receive = sleep andThen trace andThen act

  def act: Receive = {
    case Create(width, depth) => {
      // Create 'width' number of children.
      if (depth > 1) for (index <- 1 to width) {
        context.actorOf(Props[Node].withDeploy(clusterDeploy), "c"+index) ! Create(width, depth - 1)
      }
    }
    case Ping => {
      if (context.children.isEmpty) {
        context.parent ! Pong(1)
      } else {
        // Set a pongStatus.
        pongStatus = Some(PongStatus(context.children.size, 0))
        // Dispatch to children.
        context.children.foreach(_ ! Ping)
      }
    }
    case Pong(count) => {
      // Update pongStatus.
      pongStatus = pongStatus.map(_.touch(count))
      pongStatus.foreach(_ match {
        case PongStatus(0, totalCount) => {
          context.parent ! Pong(totalCount + 1)
        }
        case _ => ()
      })
    }
  }
}

object Quick extends App {

  val sys = ActorSystem("sys")
  sys.registerExtension(cluster.ClusterExtension)
  val root = sys.actorOf(Props[Node], "root")
  val deadLetters = sys.actorOf(Props(new Actor with ActorLogging {
    protected def receive = {
      case msg@DeadLetter(Pong(_), root, _) => {
        log.info("Dead Letter received. Starting a new Ping {}", msg)
        context.system.scheduler.scheduleOnce(1.second, root, Ping)
      }
    }
  }))
  sys.eventStream.subscribe(deadLetters, classOf[DeadLetter])

  root ! Create(width = 3, depth = 3)
  root ! Ping

  Console.readLine("Press enter to shut down ...")
  sys.shutdown()

}
object QuickSlave extends App {

  val sys = ActorSystem("sys")
  val monitor = sys.actorOf(Props(new Actor with ActorLogging {
      protected def receive = {
        case RemoteClientWriteFailed(msg, cause, transport, address) => {
          println("[ERROR:] Failed sending message ({}/{}) to remote: {}", msg.getClass, msg, address)
        }
      }
    }))
  sys.eventStream.subscribe(monitor, classOf[RemoteClientWriteFailed])
  sys.registerExtension(cluster.ClusterExtension)



  Console.readLine("Press enter to shut down ...")
  sys.shutdown()

}



