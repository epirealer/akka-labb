package net.gnejs.ring

import akka.actor._
import akka.util.duration._
import akka.remote.RemoteScope
import com.hazelcast.core._
import java.util.concurrent.TimeUnit
import java.util.Date

class Cluster(sys: ExtendedActorSystem, hz: HazelcastInstance) extends Extension {
  val random = new java.security.SecureRandom()
  val map = hz.getMap[Address, Date]("clustered_actor_systems")
  val myAddress = sys.provider.getExternalAddressFor(Address("akka", sys.name, "", 0))
    .getOrElse(throw new Exception("Why no address?"))

  // Register the deregistration.
  sys.registerOnTermination(detach())

  // Register the update proc.
  val cancellable = Some(sys.scheduler.schedule(0.second, 5.seconds){attach()})


  def attach(){
    println("hz: Updating " + myAddress)
    map.put(myAddress, new Date(), 7, TimeUnit.SECONDS)
  }
  def detach() {
    cancellable.foreach(_.cancel())
    map.remove(myAddress)
  }

  def clusterMembers = map.keySet()
  def randomMember: Address = {
    if (map.isEmpty) myAddress
    else {
      val list = new java.util.ArrayList[Address](map.keySet())
      java.util.Collections.shuffle(list, random)
      list.get(0)
    }
  }
}
object ClusterExtension extends ExtensionId[Cluster] with ExtensionIdProvider {
  lazy val hz = Hazelcast.getDefaultInstance
  Runtime.getRuntime.addShutdownHook(new Thread(new Runnable {
    def run() {hz.getLifecycleService.shutdown()}
  }))
  def lookup = ClusterExtension
  def createExtension(sys: ExtendedActorSystem) = new Cluster(sys, hz)
}
trait ClusterSupport { self: Actor =>
  def clusterDeploy = Deploy(scope = RemoteScope(ClusterExtension(context.system).randomMember))
}


// Messages
case object Begin
case class Packet(lapsCompleted: Int = 0, hops: Int = 0, createdAt: Long = System.currentTimeMillis())
case class NodeSetup(nextNode: ActorRef)

// A node in the ring.
class Node extends Actor with ActorLogging {
  import context._
  val random = new java.security.SecureRandom()

  var nextNode: Option[ActorRef] = None


  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.info("preRestart for {} and message {}", reason, message)
  }


  override def postRestart(reason: Throwable) {
    log.info("postRestart for {}", reason)
  }

  override def preStart() {
    log.info("Creating ring node")
  }


  override def postStop() {
    log.info("Stopping ring node")
  }

  protected def receive = {
    case NodeSetup(node) => {
      nextNode = Some(node)
      become(active)
    }
  }

  def active: Receive = {
    case p@Packet(laps, hops, createdAt) => {
      log.info("Packet received: '{}'", p)
      Thread sleep 10000
      nextNode.foreach(_ ! Packet(laps, hops + 1, createdAt))
      // There's one chance in three - that we will throw an exception!
      if (random.nextInt(3) == 1) {
        println("Me (%s) is throwing an exception".format(self.path))
        throw new Exception("Oops - I (%s) threw an exception!".format(self.path))
      }
    }
  }
}

// The root node of the actor-ring.
class RingMaster(initialNumberOfNodes: Int, numberOfLapsBeforeGrowth: Int, growthSize: Int)
  extends Actor with ActorLogging with ClusterSupport {


  override def supervisorStrategy() = OneForOneStrategy(){
    case any: Any => {
      println("----+++++++++++---- Something very bad happened to a child actor: " + any)
      SupervisorStrategy.Resume
    }
  }

  var startingNode: Option[ActorRef] = None

  override def preStart() {
    addRingNodes(initialNumberOfNodes - 1)
  }

  protected def receive = {
    case Begin => startingNode.foreach(_ ! Packet())
    case Packet(laps, hops, createdAt) => {
      val completedLaps = laps + 1
      if (completedLaps < numberOfLapsBeforeGrowth) {
        startingNode.foreach(_ ! Packet(completedLaps, hops + 1, createdAt))
      } else {
        val elapsed = System.currentTimeMillis() - createdAt
        log.info("{} laps ({} hops) completed in {} ms.", completedLaps, hops + 1, elapsed)

        addRingNodes(growthSize)
        Thread sleep 2000
        startingNode.foreach(_ ! Packet())
      }
    }
  }

  def addRingNodes(numberOfNodes: Int) {
    val startChildIdx = context.children.size + 1
    val spawned = startChildIdx.to(startChildIdx + numberOfNodes - 1).map {idx =>
      val childName = "node-"+idx
      context.actorOf(Props[Node].withDeploy(clusterDeploy),
        childName)
    }
    startingNode = Some(spawned.foldLeft(startingNode.getOrElse(self)){(nextNode, currentNode) =>
      currentNode.tell(NodeSetup(nextNode))
      currentNode
    })
  }

}

object Track {
  def apply(system: ActorSystem) {
    val monitor = system.actorOf(Props(new Actor {
      protected def receive = {
        case any => println("EE (%s)[%s] = %s".format(system.name, any.getClass.getSimpleName, any))
      }
    }))
    system.eventStream.subscribe(monitor, classOf[Any])
  }
}

// ---- Main(s) -----

object RunRing extends App {
  val system = ActorSystem("sys")
  Track(system)
  system.registerExtension(ClusterExtension)

  val master = system.actorOf(Props(new RingMaster(
    initialNumberOfNodes = 5,
    numberOfLapsBeforeGrowth = 10000,
    growthSize = 10
  )), "ringMaster")
  master ! Begin

  // Wait for input - then shutdown
  readLine("\nPress Enter to shut down ...\n")
  system.shutdown()
}

object RingWorker extends App {
  // Define the port to use.
  val system = ActorSystem("sys")
  Track(system)
  system.registerExtension(ClusterExtension)

// Wait for input - then shutdown
  readLine("\nPress Enter to shut down ...\n")
  system.shutdown()
}
